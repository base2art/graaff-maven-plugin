package com.base2art.graaff.mavenplugin;

import org.apache.maven.plugin.MojoExecutionException;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class ExecutionTest
{

  @Test
  public void shouldSomething() throws MojoExecutionException
  {
    MailReaderCodeMojo mrm = new MailReaderCodeMojo();
    mrm.setInputDirectory("src/test/resources/graaff");
    mrm.execute();
    assertThat(true).isTrue();
  }

  @Test
  public void shouldRenderViews() throws MojoExecutionException
  {
    MailReaderViewsMojo mrm = new MailReaderViewsMojo();
    mrm.setInputDirectory("src/test/resources/graaff");
    mrm.setOutputDirectory("target/test/root");
    mrm.setModulesOutputDirectory("target/test/root/modules");
    mrm.execute();
    assertThat(true).isTrue();
  }
}