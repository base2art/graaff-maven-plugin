package com.base2art.graaff.mavenplugin;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class RandomStringTest
{
    @Test
    public void shouldPrintRandomString()
    {
        System.out.println(new RandomSecretGenerator().nextSecret());
        assertThat(new RandomSecretGenerator().nextSecret().length()).isEqualTo(64);
        assertThat(new RandomSecretGenerator().nextSecret()).isNotEqualTo(new RandomSecretGenerator().nextSecret());
        final RandomSecretGenerator gen = new RandomSecretGenerator();
        assertThat(gen.nextSecret()).isNotEqualTo(gen.nextSecret());
    }
}
