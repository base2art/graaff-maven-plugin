package com.base2art.graaff.mavenplugin;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "generateViews", defaultPhase = LifecyclePhase.INITIALIZE)
public class MailReaderViewsMojo
    extends MailReaderMojoBase
{

    private static final String DefaultInputDirectory = "conf/graaff";
    private static final String DefaultOutputDirectory = ".";
    private static final String DefaultModulesOutputDirectory = "modules";
    private static final String DefaultGeneratedModulesOutputDirectory = "gen-modules";

    @Parameter(property = "generateViews.inputputDirectory",
               defaultValue = DefaultInputDirectory)
    private String inputDirectory;

    @Parameter(property = "generateViews.outputDirectory",
               defaultValue = DefaultOutputDirectory)
    private String outputDirectory;

    @Parameter(property = "generateViews.modulesOutputDirectory",
               defaultValue = DefaultModulesOutputDirectory)
    private String modulesOutputDirectory;

    @Parameter(property = "generateViews.generatedModulesOutputDirectory",
               defaultValue = DefaultGeneratedModulesOutputDirectory)
    private String generatedModulesOutputDirectory;

    @Parameter(property = "generateViews.applicationPassword")
    private String applicationPassword;

    @Parameter(property = "generateViews.applicationDomain")
    private String applicationDomain;

    @Parameter(property = "generateViews.applicationSecret")
    private String applicationSecret;

    @Component
    private MavenProject project;

    public String getApplicationDomain()
    {
        return this.valueFor(this.applicationDomain, "www.localhost.com");
    }

    public void setApplicationDomain(String applicationDomain)
    {
        this.applicationDomain = applicationDomain;
    }

    public String getApplicationPassword()
    {
        return this.valueFor(this.applicationPassword, "!asdfQWERTY1234!");
    }

    public void setApplicationPassword(String applicationPassword)
    {
        this.applicationPassword = applicationPassword;
    }

    public String getApplicationSecret()
    {
        return this.valueFor(this.applicationSecret, new RandomSecretGenerator().nextSecret());
    }

    public void setApplicationSecret(String applicationSecret)
    {
        this.applicationSecret = applicationSecret;
    }

    public String getInputDirectory()
    {
        return this.valueFor(this.inputDirectory, DefaultInputDirectory);
    }

    public void setInputDirectory(String inputDirectory)
    {
        this.inputDirectory = inputDirectory;
    }

    // PROD
    public String getOutputDirectory()
    {
        return this.valueFor(this.outputDirectory, DefaultOutputDirectory);
    }

    public void setOutputDirectory(String outputDirectory)
    {
        this.outputDirectory = outputDirectory;
    }

    // PROD
    public String getModulesOutputDirectory()
    {
        return this.valueFor(this.modulesOutputDirectory, DefaultModulesOutputDirectory);
    }

    public void setModulesOutputDirectory(String modulesOutputDirectory)
    {
        this.modulesOutputDirectory = modulesOutputDirectory;
    }

    public String getGeneratedModulesOutputDirectory()
    {
        return this.valueFor(this.generatedModulesOutputDirectory, DefaultGeneratedModulesOutputDirectory);
    }

    public void setGeneratedModulesOutputDirectory(String generatedModulesOutputDirectory)
    {
        this.generatedModulesOutputDirectory = generatedModulesOutputDirectory;
    }

    @Override
    protected void process()
        throws MojoExecutionException
    {
        this.parse(this.getInputDirectory(), this.getGeneratedModulesOutputDirectory(), CodeType.Views);

        try
        {
            this.parseSingleFile("standard-files.mail", this.getOutputDirectory());
            this.parseSingleFile("common-files.mail", new File(this.getOutputDirectory(), "modules/common").toString());
            doIncludesFor(new File(this.baseDir(), this.getOutputDirectory()));

            File[] directories = new File(this.getGeneratedModulesOutputDirectory()).listFiles(
                new FilenameFilter()
                {
                    @Override
                    public boolean accept(File current, String name)
                    {
                        return new File(current, name).isDirectory();
                    }
                });

            if (directories == null)
            {
                directories = new File[0];
            }

            for (File dir : directories)
            {
                this.parseSingleFile("module-files.mail", dir.toString());
                this.doIncludesFor(dir);
            }
        }
        catch (IOException ex)
        {
            throw new MojoExecutionException(ex.getMessage(), ex);
        }
    }

    private void doIncludesFor(File dir) throws IOException,
                                                MojoExecutionException
    {
        this.includeFilePatterns(dir, "conf/application.conf");
        this.includeFilePatterns(dir, "conf/routes");
        this.includeFilePatterns(dir, "build.sbt");
    }

    private void includeFilePatterns(final File outputDir, final String filename)
        throws
        MojoExecutionException,
        IOException

    {
        Path path = FileSystems.getDefault().getPath(outputDir.toString(), filename);
        List<String> lines = java.nio.file.Files.readAllLines(path);

        List<String> sb = new ArrayList<String>();

        final String filePatternPrefix = "FilePattern:";
        for (String line : lines)
        {
            String cleanLine = line.trim();
            if (cleanLine.startsWith(filePatternPrefix))
            {
                cleanLine = cleanLine.substring(filePatternPrefix.length());

                ScriptEngineManager factory = new ScriptEngineManager();
                ScriptEngine engine = factory.getEngineByName("JavaScript");
                Map obj;
                try
                {
                    engine.eval("var a = " + cleanLine + ";");
                    obj = (Map) engine.get("a");
                }
                catch (ScriptException ex)
                {
                    throw new MojoExecutionException("Bad json Object in File Pattern: " + cleanLine, ex);
                }

                String pattern = (String) obj.get("pattern");
                String separator = (String) obj.get("separator");
                boolean prefixWhenHasItems = false;
                if (obj.containsKey("prefixWhenHasItems"))
                {
                    prefixWhenHasItems = (boolean) (Boolean) obj.get("prefixWhenHasItems");
                }

                assert pattern != null;
                assert !pattern.equals("");
                assert separator != null;

                File parent = path.getParent().toFile();
                final FileFilter wildcardFileFilter = new WildcardFileFilter(pattern);
                File[] files = parent.listFiles(wildcardFileFilter);
                if (files.length > 0)
                {
                    if (prefixWhenHasItems)
                    {
                        sb.add(separator);
                    }

                    sb.add(this.readFile(files[0].getAbsolutePath()));

                    for (int i = 1; i < files.length; i++)
                    {
                        sb.add(separator);
                        sb.add(this.readFile(files[i].getAbsolutePath()));
                    }
                }
            }
            else
            {
                sb.add(line);
            }
        }

        java.nio.file.Files.write(path, sb);
    }

    private void parseSingleFile(final String fileName, final String outputDir)
        throws MojoExecutionException, IOException
    {
        final InputStream resourceAsStream = this.getClass()
            .getClassLoader()
            .getResourceAsStream(fileName);

        InputStreamReader inputStream = new InputStreamReader(resourceAsStream);
        this.writeFileContentByImport(this.session(), this.readFile(inputStream), outputDir, CodeType.Views);
    }

    @Override
    protected MavenProject getProject()
    {
        return this.project;
    }

    @Override
    protected String template(String rez)
    {
        rez = super.template(rez);
        rez = rez.replace("<base_path>", new File(this.baseDir(), this.getOutputDirectory()).getAbsolutePath());
        rez = rez.replace("<application_secret>", this.getApplicationSecret());
        rez = rez.replace("<application_password>", this.getApplicationPassword());
        rez = rez.replace("<application_domain>", this.getApplicationDomain());
        // TODO: HANDLE INCLUDES!
        return rez;
    }
}

//    private String mapJoiner(String joiner)
//    {
//        if (joiner.equalsIgnoreCase("eol"))
//        {
//            return "\n";
//        }
//
//        if (joiner.equalsIgnoreCase("eolx2"))
//        {
//            return "\n";
//        }
//
//        if (joiner.equalsIgnoreCase("eolx3"))
//        {
//            return "\n";
//        }
//
//        if (joiner.equalsIgnoreCase("null"))
//        {
//            return "";
//        }
//
//        return joiner;
//    }
