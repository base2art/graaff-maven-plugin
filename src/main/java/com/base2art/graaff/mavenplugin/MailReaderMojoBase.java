package com.base2art.graaff.mavenplugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;

public abstract class MailReaderMojoBase
    extends AbstractMojo
{

    protected abstract MavenProject getProject();

    @Override
    public void execute() throws MojoExecutionException
    {
        this.getLog().info("Started Code Generation");
        this.process();
        this.getLog().info("Completed Code Generation");
    }

    protected abstract void process()
        throws MojoExecutionException;

    protected void parse(String inputDirectory, String outputDirectory, CodeType type)
        throws MojoExecutionException
    {
        File folder = new File(this.baseDir(), inputDirectory);
        try
        {
            this.getLog().info("Running in: " + folder.getCanonicalPath());
        }
        catch (IOException ex)
        {
            this.getLog().warn("Failed to run import over: " + inputDirectory);
            this.getLog().warn(ex);
        }

        File[] files = this.findFiles(folder);
        int outputCount = 0;
        for (File file : files)
        {
            try
            {
                final String canonicalPath = file.getCanonicalPath();
//        this.getLog().info("Unloading Script in: " + canonicalPath);
                outputCount += this.writeFileByImport(this.session(), canonicalPath, outputDirectory, type);
            }
            catch (IOException ex)
            {
                throw new MojoExecutionException(ex.getMessage(), ex);
            }
        }

        MavenProject proj = this.getProject();
        if (proj != null)
        {
            try
            {
                final String canonicalPath = new File(this.baseDir(), outputDirectory).
                    getCanonicalPath();
                if (type == CodeType.Production)
                {
                    this.getLog().
                        info("Adding compilation source: " + canonicalPath);
                    proj.addCompileSourceRoot(canonicalPath);
                }
                else if (type == CodeType.Test)
                {
                    this.getLog().
                        info("Adding test compilation source: " + canonicalPath);
                    proj.addTestCompileSourceRoot(canonicalPath);
                }
                else if (type == CodeType.Views)
                {
//        proj.addTestCompileSourceRoot(outputDirectory);
                }
                else
                {
                    throw new RuntimeException("View Type Not Found");
                }
            }
            catch (IOException ex)
            {
                throw new MojoExecutionException(ex.getMessage(), ex);
            }
        }

        this.getLog().info(type + " Files Parsed: " + files.length);
        this.getLog().info(type + " Files Created: " + outputCount);
    }

    protected File baseDir()
    {
        MavenProject proj = this.getProject();
        if (proj == null)
        {
            return new File(".");
        }

        return proj.getBasedir();
    }

    protected Session session()
    {
        Properties props = new Properties();
        return Session.getDefaultInstance(props, null);
    }

    protected File[] findFiles(File folder)
    {
        if (!folder.exists())
        {
            return new File[0];
        }

        return folder.listFiles(new FileFilter()
        {

            public boolean accept(File file)
            {
                return file.isFile();
            }
        });
    }

    protected String readFile(String path)
        throws FileNotFoundException, IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(path));
        try
        {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null)
            {
                sb.append(line);
                sb.append('\n');
                line = br.readLine();
            }
            return sb.toString().trim();
        }
        finally
        {
            br.close();
        }
    }

    protected String readFile(InputStreamReader isr)
        throws FileNotFoundException, IOException
    {
        BufferedReader br = new BufferedReader(isr);
        try
        {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null)
            {
                sb.append(line);
                sb.append('\n');
                line = br.readLine();
            }
            return sb.toString().trim();
        }
        finally
        {
            br.close();
        }
    }

    protected void writeFile(File file, String value)
        throws IOException
    {
        final File parentFile = file.getParentFile();
        if (!parentFile.exists())
        {
            parentFile.mkdirs();
        }

        BufferedWriter bw = new BufferedWriter(new FileWriter(file));

        try
        {
            bw.write(value);
            bw.flush();
        }
        finally
        {
            bw.close();
        }
    }

    protected int writeFileByImport(Session session, String path, String outputDirectory, CodeType type)
        throws MojoExecutionException
    {
        try
        {
            return this.writeFileContentByImport(session, this.readFile(path), outputDirectory, type);
        }
        catch (IOException ex)
        {
            throw new MojoExecutionException("Failed to run code generator", ex);
        }
    }

    protected int writeFileContentByImport(Session session, String fileContent, String outputDirectory, CodeType type)
        throws MojoExecutionException
    {
        InputStream is = null;
        File outputDir = new File(this.baseDir(), outputDirectory);
        try
        {
            is = new ByteArrayInputStream(fileContent.getBytes());
            Message message = new MimeMessage(session, is);
            MimeMultipart parts = (MimeMultipart) message.getContent();

            ArrayList<BodyPart> bodyParts = new ArrayList<BodyPart>();

            for (int i = 0; i < parts.getCount(); i++)
            {
                BodyPart part = parts.getBodyPart(i);
                addIf(type, CodeType.Production, part, bodyParts);
                addIf(type, CodeType.Test, part, bodyParts);
                addIf(type, CodeType.Views, part, bodyParts);
            }

            for (BodyPart bodyPart : bodyParts)
            {
                String fileName = bodyPart.getFileName();
                File outputFile = new File(outputDir, fileName);
                final Object content = bodyPart.getContent();
                String rez = null;
                if (content instanceof InputStream)
                {
                    rez = convertToString((InputStream) content);
                }
                else if (content instanceof String)
                {
                    rez = (String) content;
                }
                else
                {
                    throw new MojoExecutionException("Un-anticipated type of BodyPart.Content");
                }

                rez = this.template(rez);

                writeFile(outputFile, rez.trim());
            }

            return bodyParts.size();
        }
        catch (IOException ex)
        {
            throw new MojoExecutionException("Failed to run code generator", ex);
        }
        catch (MessagingException ex)
        {
            throw new MojoExecutionException("Mal Formed import File", ex);
        }
        finally
        {
            try
            {
                if (is != null)
                {
                    is.close();
                }
            }
            catch (IOException ex)
            {
                throw new MojoExecutionException("Failed to run code generator", ex);
            }
        }
    }

    protected String valueFor(String primary, String backup)
    {
        return this.hasValue(primary) ? primary : backup;
    }

    private void addIf(CodeType type, CodeType targetType, BodyPart part, ArrayList<BodyPart> bodyParts)
        throws MessagingException
    {
        String contentType = part.getContentType();
        int idx = contentType.indexOf(';');
        if (idx >= 0)
        {
            contentType = contentType.substring(0, idx);
        }

        if (type == targetType && targetType.MimeType().equals(contentType))
        {
            bodyParts.add(part);
        }
    }

    protected boolean hasValue(String value)
    {
        if (value == null)
        {
            return false;
        }

        return value.trim().length() > 0;
    }

    private static String convertToString(java.io.InputStream is)
    {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    protected String template(String rez)
    {
        return rez;
    }
}
