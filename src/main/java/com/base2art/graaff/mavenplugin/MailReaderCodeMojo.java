package com.base2art.graaff.mavenplugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "generateCode", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class MailReaderCodeMojo
  extends MailReaderMojoBase
{

  private static final String DefaultInputDirectory = "src/main/resources/graaff";
  private static final String DefaultOutputDirectory = "target/generated-sources/graaff";
  private static final String DefaultTestOutputDirectory = "target/generated-test-sources/graaff";
  @Parameter(property = "generateCode.inputputDirectory", defaultValue = DefaultInputDirectory)
  private String inputDirectory;
  @Parameter(property = "generateCode.outputDirectory", defaultValue = DefaultOutputDirectory)
  private String outputDirectory;
  @Parameter(property = "generateCode.testOutputDirectory", defaultValue = DefaultTestOutputDirectory)
  private String testOutputDirectory;
//  @Parameter(property = "generateCode.playProjectOutputDirectory")
//  private String playProjectOutputDirectory;
  @Component
  private MavenProject project;

  public String getInputDirectory()
  {
    return this.valueFor(this.inputDirectory, DefaultInputDirectory);
  }

  public void setInputDirectory(String inputDirectory)
  {
    this.inputDirectory = inputDirectory;
  }

  // PROD
  public String getOutputDirectory()
  {
    return this.valueFor(this.outputDirectory, DefaultOutputDirectory);
  }

  public void setOutputDirectory(String outputDirectory)
  {
    this.outputDirectory = outputDirectory;
  }

  public String getTestOutputDirectory()
  {
    return this.valueFor(this.testOutputDirectory, DefaultTestOutputDirectory);
  }

  public void setTestOutputDirectory(String testOutputDirectory)
  {
    this.testOutputDirectory = testOutputDirectory;
  }

//  public String getViewOutputDirectory()
//  {
//    return this.valueFor(this.playProjectOutputDirectory, null);
//  }
//
//  public void setViewOutputDirectory(String playProjectOutputDirectory)
//  {
//    this.playProjectOutputDirectory = playProjectOutputDirectory;
//  }

  @Override
  protected void process()
    throws MojoExecutionException
  {
    this.parse(this.getInputDirectory(), this.getOutputDirectory(), CodeType.Production);
    this.parse(this.getInputDirectory(), this.getTestOutputDirectory(), CodeType.Test);
//    final String derivedViewOutputDirectory = this.getViewOutputDirectory();
//    if (this.hasValue(derivedViewOutputDirectory))
//    {
//      this.parse(this.getInputDirectory(), derivedViewOutputDirectory, CodeType.Views);
//    }
  }

  @Override
  protected MavenProject getProject()
  {
    return this.project;
  }
}
