//package com.base2art.graaff.mavenplugin;
//
//import java.io.*;
//import java.util.Properties;
//import javax.mail.BodyPart;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.Session;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
//import org.apache.maven.plugin.AbstractMojo;
//import org.apache.maven.plugin.MojoExecutionException;
//import org.apache.maven.plugins.annotations.LifecyclePhase;
//import org.apache.maven.plugins.annotations.Mojo;
//import org.apache.maven.plugins.annotations.Parameter;
//
//@Mojo(name = "generateViews", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
//public class MailReaderMojo extends AbstractMojo
//{
//
//  private static final String DefaultOutputDirectory = ".";
//  private static final String DefaultInputDirectory = "conf/resources/graaff";
//  @Parameter(property = "generateViews.outputDirectory", defaultValue = DefaultOutputDirectory)
//  private String outputDirectory;
//  @Parameter(property = "generateViews.inputputDirectory", defaultValue = DefaultInputDirectory)
//  private String inputDirectory;
//
//  public String getOutputDirectory()
//  {
//    return this.hasValue(this.outputDirectory)
//      ? this.outputDirectory
//      : DefaultOutputDirectory;
//  }
//
//  public void setOutputDirectory(String outputDirectory)
//  {
//    this.outputDirectory = outputDirectory;
//  }
//
//  public String getInputDirectory()
//  {
//    return this.hasValue(this.inputDirectory)
//      ? this.inputDirectory
//      : DefaultInputDirectory;
//  }
//
//  public void setInputDirectory(String inputDirectory)
//  {
//    this.inputDirectory = inputDirectory;
//  }
//
//  @Override
//  public void execute() throws MojoExecutionException
//  {
//
//    this.getLog().info("Starting Code Generation");
//    File[] messageFiles = this.findFiles();
//
//    Properties props = new Properties();
//    Session session = Session.getDefaultInstance(props, null);
//    int count = 0;
//    for (File file : messageFiles)
//    {
//      count += WriteFilesByImport(session, file.getPath());
//    }
//
//    this.getLog().info("Files Parsed: " + messageFiles.length);
//    this.getLog().info("Files Created: " + count);
//  }
//
//  private int WriteFilesByImport(Session session, String path) throws MojoExecutionException
//  {
//    InputStream is = null;
//    File outputDir = new File(this.getOutputDirectory());
//    try
//    {
//      is = new ByteArrayInputStream(readFile(path).getBytes());
//      Message message = new MimeMessage(session, is);
//      MimeMultipart parts = (MimeMultipart) message.getContent();
//      for (int i = 0; i < parts.getCount(); i++)
//      {
//        BodyPart part = parts.getBodyPart(i);
//        String fileName = part.getFileName();
//        File outputFile = new File(outputDir, fileName);
//        this.writeFile(outputFile, (String) part.getContent());
//      }
//      
//      return parts.getCount();
//    }
//    catch (IOException ex)
//    {
//      throw new MojoExecutionException("Failed to run code generator", ex);
//    }
//    catch (MessagingException ex)
//    {
//      throw new MojoExecutionException("Mal Formed import File", ex);
//    }
//    finally
//    {
//      try
//      {
//        if (is != null)
//        {
//          is.close();
//        }
//      }
//      catch (IOException ex)
//      {
//        throw new MojoExecutionException("Failed to run code generator", ex);
//      }
//    }
//  }
//
//  private boolean hasValue(String value)
//  {
//    if (value == null)
//    {
//      return false;
//    }
//
//    return value.trim().length() > 0;
//  }
//
//  private File[] findFiles()
//  {
//    File folder = new File(this.getInputDirectory());
//    if (!folder.exists())
//    {
//      return new File[0];
//    }
//
//    return folder.listFiles(new FileFilter()
//    {
//
//      public boolean accept(File file)
//      {
//        return file.isFile();
//      }
//    });
//  }
//
//  private static String readFile(String path)
//    throws FileNotFoundException, IOException
//  {
//    BufferedReader br = new BufferedReader(new FileReader(path));
//    try
//    {
//      StringBuilder sb = new StringBuilder();
//      String line = br.readLine();
//
//      while (line != null)
//      {
//        sb.append(line);
//        sb.append('\n');
//        line = br.readLine();
//      }
//      return sb.toString().trim();
//    }
//    finally
//    {
//      br.close();
//    }
//  }
//
//  private void writeFile(File file, String value)
//    throws IOException
//  {
//    final File parentFile = file.getParentFile();
//    if (!parentFile.exists())
//    {
//      parentFile.mkdirs();
//    }
//
//    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
//
//    try
//    {
//      bw.write(value);
//      bw.flush();
//    }
//    finally
//    {
//      bw.close();
//    }
//  }
//}
