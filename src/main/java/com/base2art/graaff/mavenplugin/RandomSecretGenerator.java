package com.base2art.graaff.mavenplugin;

import java.security.SecureRandom;
import org.apache.commons.codec.binary.Hex;

public final class RandomSecretGenerator
{

    private final SecureRandom random = new SecureRandom();

    public String nextSecret()
    {
        byte[] bytes = new byte[32];
        random.nextBytes(bytes);
        return new String(Hex.encodeHex(bytes));
    }
}
