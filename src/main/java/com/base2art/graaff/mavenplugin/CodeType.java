package com.base2art.graaff.mavenplugin;

public enum CodeType
{

  Production("text/x-java-source-prod"),
  Test("text/x-java-source-test"),
  Views("text/scala+html");
  private final String mimeType;

  private CodeType(String mimeType)
  {
    this.mimeType = mimeType;
  }

  public String MimeType()
  {
    return this.mimeType;
  }
}
